<?php

// Chargement des classes
require_once '../vendor/autoload.php';

// Importation des classes
use App\API\Core\Router;
use App\API\Core\Container;

// Routage
$router = new Router();
//echo '<pre>';var_dump($router->getRoute());echo '</pre>';

// Container
$container = new Container();

// Contrôleur
$controller = $container->get(
    $router->getRoute()['route']['controller']
);

// Méthode
$method = $router->getRoute()['route']['method'];

// Variables d'URL
$uriVars = $router->getRoute()['uriVars'];

// Appel de la méthode
$controller->$method($uriVars);
// var_dump($controller);

// Dotenv
/*$dotenv = $container->get('core.dotenv');
echo '<pre>';var_dump($dotenv->get('db_host')); echo '</pre>';*/

// Database
$database = $container->get('core.database');
//echo '<pre>';var_dump($database->connect()); echo '</pre>';

?>