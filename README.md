# M1 Web Evaluation

## Objectif

Dans les migrations, ajouter la création d’une table nommée `country` contenant les colonnes

* **id** représentant la clé primaire

* **name** pour le nom du pays

* **city_id** pour lier un pays et sa capitale; clé étrangère ciblant la clé primaire de la table `city`

Dans l’API

* créer la route `/countries`

* créer le contrôleur `CountryController`

* créer l’entité `Country` et la classe de dépôt `CountryRepository`

Dans la partie publique de l’application

* créer la route `/countries`

* créer une vue dans un dossier nommé `country`

La vue doit appeler la route `/countries` de l’API et afficher les pays, ainsi que leur capitale, en HTML

## Lancer l'API et le Frontal

Le patron de conception **Contrôleur frontal** permet de définir un point d’entrée unique dans l’application

Avec le serveur intégré du PHP, pour lancer le contrôleur frontal en ligne de commandes

```bash
php -S localhost:8001 -t api
php -S localhost:8000 -t public
```

## Résultat API (Screenshot)

![API](https://cdn.discordapp.com/attachments/598963656130297858/635123223561895936/APIweb.PNG "API")

## Résultat Frontal (Screenshot)

![Front](https://cdn.discordapp.com/attachments/598963656130297858/635123572834041886/FrontWeb.PNG "Front")