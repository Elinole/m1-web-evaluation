<?php

namespace App\Front\Controller;

use App\Front\Controller\AbstractController;

class HomepageController extends AbstractController {
    public function index(array $uriVars = []) {
        //echo 'homepage' . $uriVars['id'];
        //echo 'homepage';
        // Extract convertit les clés d'un array en variable
        //extract($uriVars);
        //require_once __DIR__ . '/../../../templates/homepage/index.php';
        $date = new \DateTime();
        $this->render('homepage/index', [
            'id' => $uriVars['id'],
            'date' => $date->format('d/m/Y')
        ]);
    }
}

?>