<?php

namespace App\Front\Controller;

use App\API\Repository\CountryRepository;
use App\Front\Controller\AbstractController;


class CountriesController extends AbstractController {
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository) {
        $this->countryRepository = $countryRepository;
    }

    public function index(array $uriVars = []) {
        $this->render('country/index', [
            'countries' => $this->countryRepository->findAllWithCities(),
        ]
        );
    }
}

?>