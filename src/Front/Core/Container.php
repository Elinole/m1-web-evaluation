<?php

namespace App\Front\Core;

class Container {
    private $services = [];

    public function get(string $idService) {
        $this->services = [
            'controller.homepage' => function() { 
                return new \App\Front\Controller\HomepageController();
            },
            'controller.not.found' => function() { 
                return new \App\Front\Controller\NotFoundController();
            },
            'core.dotenv' => function() { 
                return new \App\API\Core\Dotenv();
            },
            'core.database' => function() { 
                return new \App\API\Core\Database(
                    $this->services['core.dotenv']()
                );
            },
            'controller.countries' => function() { 
                return new \App\Front\Controller\CountriesController(
                    $this->services['repository.country']()
                );
            },
            'repository.country' => function() { 
                return new \App\API\Repository\CountryRepository(
                    $this->services['core.database']()
                );
            },
        ];

        return $this->services[$idService]();
    }
}

?>