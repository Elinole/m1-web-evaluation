<?php

namespace App\API\Migrations;

use App\API\Migrations\AbstractMigrations;

require_once 'vendor/autoload.php';

class Migrations20191019 extends AbstractMigrations {
    protected $sql = "
        INSERT INTO destination.city
        VALUES 
        (NULL, 'Paris', 'paris.jpg'),
        (NULL, 'Tokyo', 'tokyo.jpg'),
        (NULL, 'Londres', 'londres.jpg');
    ";
}

?>