<?php

namespace App\API\Controller;

use App\API\Repository\CountryRepository;
use App\API\Controller\AbstractController;


class CountriesController extends AbstractController {
    private $countryRepository;

    public function __construct(CountryRepository $countryRepository) {
        $this->countryRepository = $countryRepository;
    }

    public function index(array $uriVars = []) {
        $this->render([
            'countries' => $this->countryRepository->findAllWithCities()
        ]);
    }
}

?>