<?php

namespace App\API\Repository;

use App\API\Core\Database;

class CountryRepository {
    private $database;
    private $connection;

    public function __construct(Database $database) {
        $this->database = $database;
        $this->connection = $this->database->connect();
    }

    public function findAll():array {
        $sql = "
            SELECT country.*
            FROM destination.country;
        ";
        $query = $this->connection->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(\PDO::FETCH_CLASS, 'App\API\Entity\Country');
        return $results;
    }

    public function findAllWithCities():array {
        $sql = "
            SELECT country.*, city.name AS city_name
            FROM destination.country, destination.city
            WHERE country.city_id = city.id;
        ";
        $query = $this->connection->prepare($sql);
        $query->execute();
        $results = $query->fetchAll(\PDO::FETCH_CLASS, 'App\API\Entity\Country');
        return $results;
    }
}

?>