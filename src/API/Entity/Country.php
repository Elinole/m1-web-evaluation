<?php

namespace App\API\Entity;

class Country implements \JsonSerializable {
    private $id;
    private $name;
    private $city_id;
    private $city_name;

    public function jsonSerialize():array {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'cities' => [
                'city_id' => $this->getCityId(),
                'city_name' => $this->getCityName(),
            ]
        ];
    }

    public function getId():int { return $this->id; }
    public function setId(int $id):void {
        $this->id = $id;
     }

    public function getName():string { return $this->name; }
    public function setName(string $name):void {
        $this->name = $name;
     }

    public function getCityId():int { return $this->city_id; }
    public function setCityId(int $city_id):void {
        $this->city_id = $city_id;
     }

    public function getCityName():string { return $this->city_name; }
    public function setCityName(string $city_name):void {
        $this->city_name = $city_name;
     }
}

?>