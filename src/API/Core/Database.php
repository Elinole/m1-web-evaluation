<?php

namespace App\API\Core;

use App\API\Core\Dotenv;

class Database {
    private $connection;
    private $dotEnv;

    public function __construct(Dotenv $dotenv) {
        $this->dotEnv = $dotenv;
        $this->connection = new \PDO(
            "mysql:host={$this->dotEnv->get('db_host')};dbname={$this->dotEnv->get('db_name')};charset=utf8",
            $this->dotEnv->get('db_user'),
            $this->dotEnv->get('db_pwd'),
            [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
            ]
        );
    }

    public function connect():\PDO {
        return $this->connection;
    }
}

?>