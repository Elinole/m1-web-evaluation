<?php

// Chargement des classes
require_once '../vendor/autoload.php';

// Importation des classes
use App\Front\Core\Router;
use App\Front\Core\Container;

// Routage
$router = new Router();
//echo '<pre>';var_dump($router->getRoute());echo '</pre>';

// Container
$container = new Container();

// Contrôleur
$controller = $container->get(
    $router->getRoute()['route']['controller']
);

// Méthode
$method = $router->getRoute()['route']['method'];

// Variables d'URL
$uriVars = $router->getRoute()['uriVars'];

// Appel de la méthode
$controller->$method($uriVars);
// var_dump($controller);

?>